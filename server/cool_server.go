package server

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"net"
	"strconv"
)

// CRUD constants
const (
	GET    = "GET"
	PUT    = "PUT"
	POST   = "POST"
	DELETE = "DELETE"
)

// PROTOCOL version
const PROTOCOL = "HTTP/1.0 "

// EOL end of line
const EOL = "\r\n"

// Common HTTP status codes
const (
	HttpOK       = PROTOCOL + "200 OK" + EOL
	HttpCreated  = PROTOCOL + "201 Created" + EOL
	HttpNotFound = PROTOCOL + "404 Not Found" + EOL
	HttpInternal = PROTOCOL + "500 Internal Server Error" + EOL
)

// Byte constants
const (
	LineFeed       = 13
	CarriageReturn = 10
	Space          = 32
	Colon          = 58
)

type Status string
type Headers map[string]interface{}

type Body string

type Request struct {
	RequestLine []string
	Headers     map[string]interface{}
	Body        string
}

type CoolServer struct {
	Host       string
	Port       string
	connection Connection
	routes     Routes
}

type Connection struct {
	net.Conn
}

type Routes struct {
	GET    map[string]func(*CoolServer)
	POST   map[string]func(*CoolServer)
	PUT    map[string]func(*CoolServer)
	DELETE map[string]func(*CoolServer)
}

var customHeaders = Headers{}

func Init(host, port string) *CoolServer {
	getPaths := make(map[string]func(*CoolServer))
	postPaths := make(map[string]func(*CoolServer))
	putPaths := make(map[string]func(*CoolServer))
	deletePaths := make(map[string]func(*CoolServer))

	return &CoolServer{
		Host:       host,
		Port:       port,
		connection: Connection{},
		routes:     Routes{getPaths, postPaths, putPaths, deletePaths},
	}
}

func (cs *CoolServer) Get(path string, handler func(*CoolServer)) {
	if path == "" {
		spew.Dump("missing path")
		return
	}

	if handler == nil {
		spew.Dump("missing handler")
		return
	}
	cs.routes.GET[path] = handler
}

func (cs *CoolServer) Post(path string, handler func(*CoolServer)) {
	if path == "" {
		spew.Dump("missing path")
		return
	}

	if handler == nil {
		spew.Dump("missing handler")
		return
	}
	cs.routes.POST[path] = handler
}

func (cs *CoolServer) Put(path string, handler func(*CoolServer)) {
	if path == "" {
		spew.Dump("missing path")
		return
	}

	if handler == nil {
		spew.Dump("missing handler")
		return
	}
	cs.routes.PUT[path] = handler
}

func (cs *CoolServer) Delete(path string, handler func(*CoolServer)) {
	if path == "" {
		spew.Dump("missing path")
		return
	}

	if handler == nil {
		spew.Dump("missing handler")
		return
	}
	cs.routes.DELETE[path] = handler
}

func (cs *CoolServer) Connect() {
	listener, err := net.Listen("tcp", cs.Host+":"+cs.Port)
	if err != nil {
		panic(err)
	}
	defer listener.Close()

	fmt.Printf("Listening on host: %s, port: %s\n", cs.Host, cs.Port)

	for {
		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}

		cs.connection.Conn = conn

		go func(conn net.Conn) {
			buf := make([]byte, 1024)
			_, err := conn.Read(buf)
			if err != nil {
				fmt.Printf("Error reading: %#v\n", err)
				return
			}

			cs.parseRequest(buf)
		}(cs.connection)
	}
}

func (cs *CoolServer) parseRequest(buf []byte) Request {
	var request Request
	request.RequestLine, buf = cs.readRequestLine(buf)
	request.Headers, buf = cs.readHeaders(buf)
	request.Body = cs.readBody(buf)

	method := request.RequestLine[0]
	path := request.RequestLine[1]

	switch method {
	case GET:
		cs.callHandler(cs.routes.GET, path)
	case POST:
		cs.callHandler(cs.routes.POST, path)
	case PUT:
		cs.callHandler(cs.routes.PUT, path)
	case DELETE:
		cs.callHandler(cs.routes.DELETE, path)
	}

	return request
}

func (cs *CoolServer) callHandler(handlers map[string]func(server *CoolServer), path string) {
	handler, exists := handlers[path]
	if !exists {
		cs.Response(HttpNotFound, "Not found")
		return
	}

	handler(cs)
}

func (cs *CoolServer) readRequestLine(buf []byte) ([]string, []byte) {
	var bytesRead []byte
	var firstLine []string
	var lastByteRead byte

	for i, b := range buf {
		// First line has been read
		if lastByteRead == LineFeed && b == CarriageReturn {
			buf = buf[i:]
			firstLine = append(firstLine, string(bytesRead))
			break
		}
		// Spaces is used as a delimiter in first line
		if b == Space {
			firstLine = append(firstLine, string(bytesRead))
			bytesRead = bytesRead[:0]
			lastByteRead = b
			continue
		}

		if b != LineFeed {
			bytesRead = append(bytesRead, b)
		}

		lastByteRead = b
	}

	return firstLine, buf
}

func (cs *CoolServer) readHeaders(buf []byte) (map[string]interface{}, []byte) {
	var bytesRead []byte
	headers := make(map[string]interface{})
	var lastByteRead byte

	var key string
	var value string
	readingKey := true

	for i, b := range buf {
		// End of headers
		if lastByteRead == LineFeed && b == CarriageReturn {
			buf = buf[i+2:]
			break
		}

		// Reading key
		if b == Colon && readingKey {
			key = string(bytesRead)
			readingKey = false
			lastByteRead = b
			bytesRead = bytesRead[:0]
			continue
		}

		// Reading value
		if lastByteRead == LineFeed && b == CarriageReturn {
			value = string(bytesRead)
			headers[key] = value
			readingKey = true
			bytesRead = bytesRead[:0]
		}

		// Don't append these bytes
		if b == CarriageReturn || b == LineFeed || b == Space {
			lastByteRead = b
			continue
		}

		bytesRead = append(bytesRead, b)
		lastByteRead = b
	}

	return headers, buf
}

func (cs *CoolServer) readBody(buf []byte) string {
	// If the request doesn't contain a body, return
	if len(buf) == 0 {
		return ""
	}

	// Everything but the body is left of the request
	return string(buf)
}

func (cs *CoolServer) Headers(key, value string) {
	customHeaders[key] = value
}

func (cs *CoolServer) Response(code Status, message string) {
	//headers := Headers{"Content-Length": strconv.Itoa(len(message))}
	customHeaders["Content-Length"] = strconv.Itoa(len(message))
	// Cast the status line to string
	response := string(code)

	// Convert headers to a string
	response += prepareHeaders(customHeaders)

	// If a message is passed in the function call, add it to the response
	if message != "" {
		response += message
	}

	// Write response to client. Panic if fail
	_, err := cs.connection.Write([]byte(response))
	if err != nil {
		panic(err)
	}

	// Clear headers
	customHeaders = Headers{}

	// Close the connection to the client. Panic if fail
	if cs.connection.Close() != nil {
		panic(err)
	}

}

func prepareHeaders(headers Headers) string {
	headersString := ""
	for k, v := range headers {
		headersString += fmt.Sprintf("%s: %s%s", k, v, EOL)
	}

	return fmt.Sprintf("%s%s", headersString, EOL)
}
