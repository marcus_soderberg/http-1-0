package main

import (
	"http-1dot1/server"
)

func main() {
	s := server.Init("localhost", "8080")
	s.Get("/test", handleGet)
	s.Post("/my-cool-post", handlePost)

	s.Connect()

}

func handleGet(ctx *server.CoolServer) {
	ctx.Headers("My-Custom", "Header")
	ctx.Response(server.HttpOK, "{\"hej\": \"klienten\"}")
}

func handlePost(ctx *server.CoolServer) {
	ctx.Headers("My-Cool", "Custom-Header")
	ctx.Response(server.HttpCreated, "post!")
}
