En liten och undersökande implementation av en webbserver, byggd ovanpå en TCP-socket som implementerar HTTP/1.0.

Om du vill testa bwhöver du:
* Installera Go 1.20
* Stå i roten för repot och köra kommandot `go run main.go`
* Göra ett GET-anrop till `localhost:8080/test`

Alternativt labbar du runt med de HTTP-metoder som finns implementerade, eller varför inte implementera enga?